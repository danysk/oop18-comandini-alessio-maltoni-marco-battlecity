package model.common;

/**
 * A simple counter class that increment the counter and get his value.
 *
 */
public final class Counter {

    private int current;

    /**
     * Default constructor that initialize to 0 the counter.
     */
    public Counter() {
        this.current = 0;
    }

    /**
     * Increment of one the current counter.
     */
    public void increment() {
        this.current++;
    }

    /**
     * 
     * @return the value of the counter
     */

    public int getValue() {
        return current;

    }

}
