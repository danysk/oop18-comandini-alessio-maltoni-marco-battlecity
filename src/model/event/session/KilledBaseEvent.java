package model.event.session;

/**
 * An event that represent that the base is hit by a bullet so you lose.
 *
 */
public final class KilledBaseEvent implements SessionEvent {

}
