package model.event.session;

import model.entities.Tank;

/**
 * Event that represent the kill of a player.
 *
 */
public final class KilledPlayerEvent implements SessionEvent {

    private final Tank tank;

    /**
     * 
     * @param tank the player tank that is destroyed.
     */
    public KilledPlayerEvent(final Tank tank) {
        this.tank = tank;
    }

    /**
     * 
     * @return the player killed tank
     */
    public Tank getKilledTank() {
        return tank;
    }

}
