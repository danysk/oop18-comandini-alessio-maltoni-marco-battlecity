package model.command;

/**
 * Concept of direction used in commands to setup the movement and in game
 * entities to manage rendering direction.
 */
public enum Direction {
    /**
     * Up movement-state.
     */
    UP(0, -0.3),
    /**
     * Down movement-state.
     */
    DOWN(0, 0.3),
    /**
     * Left movement-state.
     */
    LEFT(-0.3, 0),
    /**
     * Right movement-state.
     */
    RIGHT(0.3, 0);

    private double xDirection;
    private double yDirection;

    Direction(final double xDirection, final double yDirection) {
        this.xDirection = xDirection;
        this.yDirection = yDirection;
    }

    /**
     * 
     * @return the x offset used to setup the movement.
     */
    public double getXDirection() {
        return this.xDirection;
    }

    /**
     * 
     * @return the x offset used to setup the movement.
     */
    public double getYDirection() {
        return this.yDirection;
    }
}
