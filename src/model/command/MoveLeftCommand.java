package model.command;

/**
 * A class that implements the movement of the tank in left direction.
 *
 */
public final class MoveLeftCommand extends AbstractMovementCommand implements Command {
    /**
     * Default constructor that set the direction to left.
     */
    public MoveLeftCommand() {
        super(Direction.LEFT);
    }

}
