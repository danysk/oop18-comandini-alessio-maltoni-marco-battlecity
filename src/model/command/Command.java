package model.command;

import model.entities.Tank;

/**
 * Interface extended by all command to manage the tank movement and shoot.
 * 
 */
public interface Command {
    /**
     * Execute a given command on a tank.
     * 
     * @param tank the tank where the command is execute
     */
    void execute(Tank tank);

}
