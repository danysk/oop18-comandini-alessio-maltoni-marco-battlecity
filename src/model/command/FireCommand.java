package model.command;

import model.entities.Tank;
/**
 * Fire command that allow the tank to shoot.
 */
public final class FireCommand implements Command {

    @Override
    public void execute(final Tank tank) {
        tank.shoot();

    }

}
