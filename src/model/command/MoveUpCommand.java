package model.command;

/**
 * A class that implements the movement of the tank in up direction.
 *
 */
public final class MoveUpCommand extends AbstractMovementCommand implements Command {
    /**
     * Default constructor that set the direction to up.
     */
    public MoveUpCommand() {
        super(Direction.UP);
    }
}
