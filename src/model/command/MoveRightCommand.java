package model.command;

/**
 * Command that move the tank Right.
 */
public final class MoveRightCommand extends AbstractMovementCommand implements Command {
    /**
     * Default constructor that set the direction to right.
     */
    public MoveRightCommand() {
        super(Direction.RIGHT);
    }

}
