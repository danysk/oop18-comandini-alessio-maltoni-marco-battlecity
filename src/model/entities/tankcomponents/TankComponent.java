package model.entities.tankcomponents;

/**
 * An interface that represent a tank component to attach to a tank. Who
 * implements directly this interface is a passive component that modify the
 * behavior of the tank in the world.
 *
 */
public interface TankComponent {

}
