package model.entities.tankcomponents;

import model.BulletEngine;
import model.entities.Tank;

/**
 * A component that allow a tank to shoot.
 *
 */
public class ShooterComponent implements ActiveTankComponent {

    private final BulletEngine shooter;
    private boolean shoot;
    private final Tank attacchedTank;

    /**
     *
     * @param shooter       the bullet engine to call to shoot
     * @param attacchedTank the attached tank that use this component
     */
    public ShooterComponent(final BulletEngine shooter, final Tank attacchedTank) {
        super();
        this.shooter = shooter;
        this.shoot = false;
        this.attacchedTank = attacchedTank;
    }

    /**
     * Try to shoot a bullet from the bullet engine.
     */
    @Override
    public void useComponent() {
        if (this.shoot) {
            this.shooter.addBullet(attacchedTank);
        }
        this.shoot = false;
    }

    /**
     * Call this to shoot.
     */
    public void isShooting() {
        this.shoot = true;
    }

}
