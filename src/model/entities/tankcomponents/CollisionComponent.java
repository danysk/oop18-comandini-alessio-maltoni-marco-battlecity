package model.entities.tankcomponents;

import model.World;
import model.collision.CollisionUtils;
import model.common.MovementImpl;
import model.entities.Block;
import model.entities.Tank;

/**
 * A component that handle collision of the tank.
 */
public final class CollisionComponent implements ActiveTankComponent {

    private final Tank attachedTank;
    private final World world;

    /**
     * Create a new component that handle collision. Collisions are managed very
     * rude. Is not a good work but it still working. The delivery of the project is
     * to near to refactor. Give the conception of the world to an other object is
     * ugly and unsafe but is fast to implement.
     * 
     * @param attachedTank the tank attached to the component.
     * @param world        the attached world to give the Game Entity list to handle
     *                     collision.
     */
    public CollisionComponent(final Tank attachedTank, final World world) {
        this.attachedTank = attachedTank;
        this.world = world;
    }

    /**
     * The method handle the collision with other block and tank.
     */
    @Override
    public void useComponent() {
        world.getWorldEntity().stream().filter(e -> e instanceof Block || e instanceof Tank).forEach(block -> {
            if (!(block instanceof Block && (((Block) block).getType().equals(Block.Type.GRASS)
                    || ((Block) block).getType().equals(Block.Type.ICE)))) {
                if (CollisionUtils.collide(attachedTank, block)) {
                    this.attachedTank.setMovement(new MovementImpl());
                }
            }
        });

        if (CollisionUtils.collideWithBorder(this.attachedTank)) {
            this.attachedTank.setMovement(new MovementImpl());
        }

    }

}
