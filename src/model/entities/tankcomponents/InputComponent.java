package model.entities.tankcomponents;

import java.util.Queue;

import model.command.Command;
import model.entities.Tank;

/**
 * An active component that manage input from a player on an AI.
 *
 */
public final class InputComponent implements ActiveTankComponent {
    private final Tank attachedTank;
    private final Queue<Command> commands;

    /**
     * 
     * @param commands     a list of commands to execute in the tank
     * @param attachedTank the tank attached to the component.
     */
    public InputComponent(final Queue<Command> commands, final Tank attachedTank) {
        this.attachedTank = attachedTank;
        this.commands = commands;
    }

    /**
     * Execute command on the tank.
     */
    @Override
    public void useComponent() {
        if (!commands.isEmpty()) {
            commands.poll().execute(attachedTank);
        }
    }

}
