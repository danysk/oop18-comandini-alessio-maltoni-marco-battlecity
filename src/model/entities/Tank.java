package model.entities;

import java.util.List;
import java.util.Optional;

import model.common.Dimension;
import model.common.DimensionImpl;
import model.enemy.Enemy;
import model.entities.tankcomponents.TankComponent;

/**
 * Interface that represent a tank in the world.
 */
public interface Tank extends GameEntity {
    /**
     * Default tank dimension.
     */
    Dimension DEFAULT_TANK_DIMENSION = new DimensionImpl(1.6, 1.6);

    /**
     * The tank shoot a bullet in the world if it can.
     */
    void shoot();

    /**
     * The tank components represent the tank characteristic and make the tank able
     * to extend tank behavior.
     * 
     * @return the list of tank components that represent the state of the tank
     */
    List<TankComponent> getComponents();

    /**
     * Attach a new component to the tank components list.
     * 
     * @param component a new component to attach
     * @return the actual tank to facilitate multi-component attachment.
     */
    Tank attach(TankComponent component);

    /**
     * Used to manage points purpose. Can be avoided but still useful.
     * 
     * @return the enemy type of the tank
     */
    Optional<Enemy> getType();

    /**
     * Setup the current enemy type if the tank is enemy. If is not enemy the type
     * is not setup.
     * 
     * @param type the type to setup.
     */
    void setType(Enemy type);

}
