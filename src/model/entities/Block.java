package model.entities;

import model.common.Dimension;
import model.common.DimensionImpl;

/**
 * Representing a block in the world.
 *
 */
public interface Block extends GameEntity {
    /**
     * the default block dimension.
     */
    Dimension DEFALULT_BLOCK_DIMENSION = new DimensionImpl(1, 1);
    /**
     * the default base block dimension.
     */
    Dimension BASE_BLOCK_DIMENSION = new DimensionImpl(2, 2);

    /**
     * 
     * @return the block type
     */
    Type getType();
    /**
     * Eepresent all the type of the block.
     *
     */
    enum Type {
        WALL(), IRON(), GRASS(), WATER(), ICE(), BASE(BASE_BLOCK_DIMENSION);

        private final Dimension dimension;

        Type() {
            this.dimension = DEFALULT_BLOCK_DIMENSION;
        }

        Type(final Dimension dimension) {
            this.dimension = dimension;
        }

        Dimension getDimension() {
            return dimension;
        }
    }

}
