package model.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import enums.Sprite;
import model.common.MovementImpl;
import model.common.Position;
import model.common.PositionImpl;
import model.enemy.Enemy;
import model.entities.tankcomponents.ActiveTankComponent;
import model.entities.tankcomponents.ShooterComponent;
import model.entities.tankcomponents.TankComponent;

/**
 * {@link Tank} Implementation.
 *
 */
public final class TankImpl extends AbstractGameEntity implements Tank {

    private final List<TankComponent> componentList;
    private Optional<Enemy> type;

    /**
     * Constructor that setup a new tank without type to a given position.
     * 
     * @param sprite   the sprite to setup.
     * @param position the position to setup.
     */
    public TankImpl(final Sprite sprite, final Position position) {
        super(sprite, position, new MovementImpl(), DEFAULT_TANK_DIMENSION);
        componentList = new ArrayList<>();
        this.type = Optional.empty();

    }

    /**
     * Constructor that implements a new tank in the (0,0) position.
     * 
     * @param sprite the sprite to setup.
     */
    public TankImpl(final Sprite sprite) {
        this(sprite, new PositionImpl());
    }

    @Override
    public void shoot() {
        this.componentList.stream().filter(c -> c instanceof ShooterComponent)
                .forEach(e -> ((ShooterComponent) e).isShooting());
    }

    @Override
    public List<TankComponent> getComponents() {
        return this.componentList;
    }

    @Override
    public void updateState() {
        this.componentList.stream().filter(e -> e instanceof ActiveTankComponent)
                .forEach(c -> ((ActiveTankComponent) c).useComponent());
        super.updateState();
        this.setMovement(new MovementImpl());
    }

    @Override
    public Tank attach(final TankComponent component) {
        this.componentList.add(component);
        return this;
    }

    @Override
    public Optional<Enemy> getType() {
        return this.type;
    }

    @Override
    public void setType(final Enemy power) {
        this.type = Optional.of(power);

    }

}
