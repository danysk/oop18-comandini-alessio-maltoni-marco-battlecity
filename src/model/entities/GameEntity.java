package model.entities;

import model.command.Direction;
import model.common.Dimension;
import model.common.Movement;
import model.common.Position;

import java.util.Optional;

import enums.Sprite;

/**
 * Interface that represent all the object in the world.
 *
 */
public interface GameEntity {
    /**
     * @return actual Game entity {@link Position}
     */
    Position getActualPosition();

    /**
     * 
     * @return actual GameEntity {@link Movement}
     */
    Movement getActualMovement();

    /**
     * Set actual GameEntity {@link Movement}.
     * 
     * @param newMovement the movement to setup
     */
    void setMovement(Movement newMovement);

    /**
     * Set actual GameEntity {@link Position}.
     * 
     * @param initialPosition the position to setup
     */
    void setPosition(Position initialPosition);

    /**
     * Update actual state of the GameEntity.
     */
    void updateState();

    /**
     * This method is used for rendering purpose.
     * 
     * @return the sprite of the GameEntity
     */
    Sprite getSprite();

    /**
     * Modify actual sprite to handle sprite modification.
     * 
     * @param sprite the new sprite to setup.
     */
    void setSprite(Sprite sprite);

    /**
     * 
     * @return the dimension of the GameEntity
     */
    Dimension getDimension();

    /**
     * 
     * @return the direction of the Game Entity in the world;
     */
    Optional<Direction> getDirection();

    /**
     * Set actual direction for rendering purpose.
     * 
     * @param direction the direction to setup.
     */
    void setDirection(Direction direction);
}
