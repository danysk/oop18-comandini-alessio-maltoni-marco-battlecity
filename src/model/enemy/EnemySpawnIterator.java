package model.enemy;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.common.Position;
import model.common.PositionImpl;

/**
 * An infinite iterator that return 3 spawn points for enemy.
 *
 */
public final class EnemySpawnIterator implements Iterator<Position> {
    private static final Position ENEMY_FIRST_SPAWN = new PositionImpl(0.0, 0.0);
    private static final Position ENEMY_SECOND_SPAWN = new PositionImpl(12.1, 0.0);
    private static final Position ENEMY_THIRD_SPAWN = new PositionImpl(24.3, 0.0);

    private final List<Position> enemySpawn = Stream.of(ENEMY_FIRST_SPAWN, ENEMY_SECOND_SPAWN, ENEMY_THIRD_SPAWN)
            .collect(Collectors.toList());
    private Iterator<Position> iterator = enemySpawn.iterator();

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Position next() {
        if (!iterator.hasNext()) {
            iterator = enemySpawn.iterator();
        }
        return new PositionImpl(iterator.next());
    }
}
