package model.collision;

import model.World;
import model.common.Movement;
import model.common.Position;
import model.entities.GameEntity;

/**
 * A Static class that implement a simple algorithm to manage the game
 * collision. Algorithm is not perfect but still working.
 *
 *
 */
public final class CollisionUtils {
    private CollisionUtils() {
    }

    /**
     * Algorithm that define if a entity is colliding with an other one. It's based
     * on the fact the game entity in the game are all square or rectangle. It's an
     * ugly method to rectify
     * 
     * @param firstEntity  the first entity to check collision with
     * @param secondEntity the second entity to check collision with
     * @return true if the first entity collide with the second
     */
    public static boolean collide(final GameEntity firstEntity, final GameEntity secondEntity) {
        /*
         * a---b //Is the first entity c---d //Is the second entity | | | | c---d e---f
         */
        final Position pos = firstEntity.getActualPosition();
        final Movement mov = firstEntity.getActualMovement();
        double ax, ay, bx, by, cx, cy, dx, dy;
        final double xFirstOffset = firstEntity.getDimension().getWidth();
        final double yFirstOffset = firstEntity.getDimension().getHeight();
        ax = pos.getX() + mov.getXMovement();
        ay = pos.getY() + mov.getYMovement();
        bx = ax + xFirstOffset;
        by = ay;
        cx = ax;
        cy = ay + yFirstOffset;
        dx = bx;
        dy = cy;

        double ex, ey;
        ex = secondEntity.getActualPosition().getX();
        ey = secondEntity.getActualPosition().getY();
        final double xSecondOffset = secondEntity.getDimension().getWidth();
        final double ySecondOffset = secondEntity.getDimension().getWidth();

        return // MainPoints
        (ax > ex && ax < ex + xSecondOffset) && (ay > ey && ay < ey + ySecondOffset)
                || (bx > ex && bx < ex + xSecondOffset) && (by > ey && by < ey + ySecondOffset)
                || (cx > ex && cx < ex + xSecondOffset) && (cy > ey && cy < ey + ySecondOffset)
                || (dx > ex && dx < ex + xSecondOffset) && (dy > ey && dy < ey + ySecondOffset);
        // Middle points //TO Implements
        // ||((ax+(xFirstOffset / 2) > ex && ax+(xFirstOffset / 2) < ex + xSecondOffset)

    }

    /**
     * Check if the game entity is in the game boundaries. It's an ugly method to
     * rectify
     * 
     * @param gameEntity the entity to check the collision with the boundaries.
     * @return true if the entity collide with the boundaries or is out of that.
     */
    public static boolean collideWithBorder(final GameEntity gameEntity) {
        final Position pos = gameEntity.getActualPosition();
        final Movement mov = gameEntity.getActualMovement();
        final double xFirstOffset = gameEntity.getDimension().getWidth();
        final double yFirstOffset = gameEntity.getDimension().getWidth();
        double ax, ay, bx, cy;
        ax = pos.getX() + mov.getXMovement();
        ay = pos.getY() + mov.getYMovement();
        bx = ax + xFirstOffset;
        cy = ay + yFirstOffset;

        return (ax < 0 || ay < 0 || bx > World.HEIGHT_BORDER || cy > World.WEIGHT_BORDER);

    }

}
