package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import enums.Sprite;
import model.common.Position;
import model.common.PositionImpl;
import model.enemy.EnemySpawnIterator;
import model.entities.Block;
import model.entities.BlockImpl;
import model.entities.Bullet;
import model.entities.GameEntity;
import model.entities.Tank;
import model.entities.tankcomponents.CollisionComponent;
import model.entities.tankcomponents.ShooterComponent;
import model.event.session.EnemyDestroyedEvent;
import model.event.session.KilledBaseEvent;
import model.event.session.KilledPlayerEvent;
import model.event.session.SessionEvent;
import model.event.world.BlockDestroyEvent;
import model.event.world.BulletDestroyEvent;
import model.event.world.TankDestroyEvent;
import model.event.world.WorldEvent;

/**
 * The world of the game. It contains all entities, boundaries and physics
 * mechanism.
 *
 */
public class World {
    /**
     * Max height limit that game entity can reach.
     */
    public static final double HEIGHT_BORDER = 26;
    /**
     * Max weight limit that game entity can reach.
     */
    public static final double WEIGHT_BORDER = 26;
    private static final Position DEF_BASE_POSITION = new PositionImpl(12.0, 24.0);

    private List<Tank> playersTank;
    private List<Block> blocks;
    private final List<Tank> enemyTank;
    private final List<Bullet> bullets;
    private final Block base;
    private Iterator<Position> enemyPositions;
    private final List<WorldEvent> events;
    private final BulletEngine bulletEngine;
    private List<SessionEvent> sessionEvents;

    /**
     * Default constructor.
     */
    public World() {
        this.base = new BlockImpl(Sprite.BASE, DEF_BASE_POSITION, Block.Type.BASE);
        this.events = new LinkedList<>();
        this.playersTank = new ArrayList<>();
        this.blocks = new ArrayList<>();
        this.enemyTank = new ArrayList<>();
        this.bullets = new ArrayList<>();
        this.enemyPositions = new EnemySpawnIterator();
        this.bulletEngine = new BulletEngine(this);
    }

    /**
     * 
     * @param map           a list of blocks that define the map in the world
     * @param playersTank   a list of tank controlled by the players
     * @param sessionEvents a list of event to notify to the game session
     */
    public void setup(final List<Tank> playersTank, final List<Block> map, final List<SessionEvent> sessionEvents) {
        playersTank
                .forEach(t -> t.attach(new ShooterComponent(bulletEngine, t)).attach(new CollisionComponent(t, this)));
        this.playersTank = playersTank;
        this.blocks = map;
        this.sessionEvents = sessionEvents;
        this.enemyPositions = new EnemySpawnIterator();
    }

    /**
     * 
     * @return a list of game entities for rendering and update purpose
     */
    public List<GameEntity> getWorldEntity() {
        return Stream
                .concat(Stream.concat(Stream.concat(playersTank.stream(), bullets.stream()),
                        Stream.concat(enemyTank.stream(), blocks.stream())), Stream.of(base))
                .collect(Collectors.toList());
    }

    /**
     * Update the current state of all the game object in the game.
     */
    public void updateState() {
        this.getWorldEntity().stream().forEach(GameEntity::updateState);
    }

    /**
     * 
     * @return the enemy now in the world
     */
    public List<Tank> getEnemy() {
        return this.enemyTank;
    }

    /**
     * As a new enemy in the world.
     * 
     * @param enemyTank is a new enemy to add in the world.
     */
    public void addEnemy(final Tank enemyTank) {
        enemyTank.setPosition(enemyPositions.next());
        enemyTank.attach(new ShooterComponent(bulletEngine, enemyTank)).attach(new CollisionComponent(enemyTank, this));
        this.enemyTank.add(enemyTank);

    }

    /**
     * Notify word of a given event.
     * 
     * @param event an event to handle by the world
     */
    public void notifyEvent(final WorldEvent event) {
        this.events.add(event);
    }

    /**
     * Process every event in the queue.
     */
    public void processEvents() {
        this.events.forEach(e -> {
            if (e instanceof BulletDestroyEvent) {
                this.bulletEngine.removeBullet(((BulletDestroyEvent) e).getBullet());
                this.bullets.remove(((BulletDestroyEvent) e).getBullet());
            }

            if (e instanceof TankDestroyEvent) {
                if (this.playersTank.contains(((TankDestroyEvent) e).getHitTank())) { // Se è un tank player notifico la
                                                                                   // session
                    this.sessionEvents.add(new KilledPlayerEvent(((TankDestroyEvent) e).getHitTank()));
                }
                if (this.enemyTank.contains(((TankDestroyEvent) e).getHitTank())
                        && !enemyTank.contains(((TankDestroyEvent) e).getSourceBullet().getAttachedTank())) {
                    // Se è un nemico e non è un altro nemico che ha sparato il proiettile (ossia
                    // chi ha sparato è un player) elimino il nemico
                    this.sessionEvents
                            .add(new EnemyDestroyedEvent(((TankDestroyEvent) e).getSourceBullet().getAttachedTank(),
                                    ((TankDestroyEvent) e).getHitTank().getType()));
                    enemyTank.remove(((TankDestroyEvent) e).getHitTank());

                }
            }
            if (e instanceof BlockDestroyEvent) { // Se è un blocco lo elimino a meno che non sia la base
                if (!((BlockDestroyEvent) e).getBlock().getType().equals(Block.Type.BASE)) {
                    this.blocks.remove(((BlockDestroyEvent) e).getBlock());
                } else {
                    ((BlockDestroyEvent) e).getBlock().setSprite(Sprite.BASE_DESTROYED); // Imposto lo sprite della base
                                                                                         // rotta
                    this.sessionEvents.add(new KilledBaseEvent()); // Invio un evento di eliminazione della base al game
                                                                   // session che imposterà il gameover;
                }
            }
        });
        events.clear();
    }

    /**
     * ad a bullet to the list of the current bullets in the world.
     * 
     * @param bullet the bullet to add
     */
    public final void addBullet(final Bullet bullet) {
        this.bullets.add(bullet);
    }

}
