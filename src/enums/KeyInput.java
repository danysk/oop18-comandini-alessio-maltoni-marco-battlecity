package enums;

/**
 * Enumeration for the Keyboard input.
 */
public enum KeyInput {

    /**
     * Keyboard input for moving up player one.
     */
    UP_PLAYER_ONE,
    /**
     * Keyboard input for moving right player one.
     */
    RIGHT_PLAYER_ONE,
    /**
     * Keyboard input for moving down player one.
     */
    DOWN_PLAYER_ONE,
    /**
     * Keyboard input for moving left player one.
     */
    LEFT_PLAYER_ONE,
    /**
     * Keyboard input for fire player one.
     */
    FIRE_PLAYER_ONE,
    /**
     * Keyboard input for moving up player two.
     */
    UP_PLAYER_TWO,
    /**
     * Keyboard input for moving right player two.
     */
    RIGHT_PLAYER_TWO,
    /**
     * Keyboard input for moving down player two.
     */
    DOWN_PLAYER_TWO,
    /**
     * Keyboard input for moving left player two.
     */
    LEFT_PLAYER_TWO,
    /**
     * Keyboard input for fire player two.
     */
    FIRE_PLAYER_TWO;

}
