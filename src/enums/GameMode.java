package enums;

/**
 * This is the enumeration for the mode of the game.
 */
public enum GameMode {

    /**
     * The construction game mode.
     */
    CONSTRUCTION,
    /**
     * Game mode not selected yet.
     */
    INDEFINITE,
    /**
     * The game mode with one player.
     */
    ONE_PLAYER,
    /**
     * The game mode with two player.
     */
    TWO_PLAYER;

}
